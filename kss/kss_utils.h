#ifndef KSS_UTILS_H
#define KSS_UTILS_H

void  ks_red_1( int *edge_list, int *edge_order, double **coeffs,int m,int k,int **inactive,int **matched, int *order,int *degs,int d0_starts,int d0_ends,int *rule_nums, double *S,int max_ni,int **edge_ids,int *deg_limit);
void  ks_red_2( int *edge_list, int *edge_order, double **coeffs,int m,int k, int *order,int *degs,int d1_starts,int d1_ends,int *h_pos1,int *h_pos2, double *S,int max_ni, int *f_appear);
int  pm_drive(int *e_list,int *edge_order,int n,int m,int k,int **matched,int *degs,int *dim_starts,int *dim_ends,int sc,int **match_set);
void read_tensor_kss(char *filename,int *n,int *m, int *k,int **edge_list,int **edge_order, int **degs,int **dim_starts,int **dim_ends,int o);
void  sk_one( int *edge_list, int *edge_order, double **coeffs,int m,int steps, double *m_val, int *h_pos,int k, int *degs, int max_ni, double *S,int offset,int *dim_starts,int *dim_ends,int *order);
void split_ijz(int *edge_list,int **edge_order,int *m,int *matched, int k,int **degs,int deg_limit);
void split_ijz_2(int **edge_list,int **edge_order,int *m,int h1,int h2,int k,int **degs,int r2a,int r2b,int index_r, int *order, int *dim_starts, int *dim_ends,int **S_stack,int *c_stack);
void  step_preprocess(int k, int *inactive, int **dim_starts,int **dim_ends, int **order, double **coeffs, int **f_appear,int *max_ni,int *degs);

#endif