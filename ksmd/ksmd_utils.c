#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include "ksmd_utils.h"

void updateDegs(int h, int *v_hids, int *v_starts, int *h_begs, int *h_points, int *matched, int **degs,int **d1lst, int *_d1st, int *_d1end,int *active) 
{   	
	int d1st = *_d1st, d1end = *_d1end;
	int i,x,j,z,q,p,c_h,x2,x3,t_h, count;

	for (j= h_begs[h]; j<h_begs[h+1];++j){  // we need to examine all endpoints of a hyperedge
		x = h_points[j];   //for the j-th endpoint
	    for (i= v_starts[x]; i<v_starts[x+1];++i){ //consider all its edges
	     	c_h = v_hids[i];   //the i-th hyperedge
	      	if (active[c_h]==1){  //if it is active, we need to examine it, otherwise it contains a matched node anyways, so it must have been examined before (or not if its a matched one)
				active[c_h]=0;  //first set it non-active 
				for (z = h_begs[c_h]; z< h_begs[c_h + 1]; ++z){ //for each endpoint of the now-defunct edge
					x2 = h_points[z];   //get its z-th  endpoint
					(*degs)[x2]--;   //reduce degree (this happens only once due to the active array and it is done so to escape things like h : (a,b,c)  and there exists edge (a,b,q)
				    if((*degs)[x2]==1 && x2!=x){ //if it's degree is 1, possible candidate  for KS-reduction. (ignore x  for a little more efficiency)
		    			for (q= v_starts[x2] ; q < v_starts[x2 + 1]; ++q){  //iterate over its endpoints to find the single active hyperedge remaining
		        			t_h = v_hids[q]; //q-th hyperedge
		      				if (active[t_h]){ //we found it
		      					count = 0;  
								for (p = h_begs[t_h]; p< h_begs[t_h + 1]; ++p){ //find the number of degree-1 endpoints in t_h
									x3=h_points[p];
									if ((*degs)[x3]== 1){ 
										count++;									
									}  
								} // if for t_h at least d-1 out of d are  of degree 1 
								if ((count >= h_begs[t_h + 1] - h_begs[t_h] - 1) ){
			  						(*d1lst)[++d1end] = t_h; //add it to the stack (and potentially clean it when it is examined then)
			  					} 
			  					break;
			  				}							
			  			}
			  		}				
			  	}			
			}		
		}		
	}
	*_d1st = d1st;
	*_d1end = d1end;
}



int ksmd(int *edge_list, int  n, int m, int *v_hids, int *v_starts, int *h_begs, int *h_points,int  **match,int **match_set)
{
	int *active = malloc(m*sizeof(int)); 
	int *degs= malloc(n*sizeof(int));
	double  max_coeff,temp_coeff;
	int curr,max_index, swap_temp;      
	int s_matches=0;
	int * d1lst;
	int matches=0;
	int i,h,j,x,count,x1,x2,randEdgeSt=0, d1st,d1end,flag,valid;
	
	d1lst= (int *) calloc(n+3, sizeof(int));/*a node can add at most one hyperedge*/
	d1st = 0; d1end = -1;
	for (i = 0; i < n; i++) { //initialize  degree // match array
	   	degs[i] =(v_starts[i+1] - v_starts[i]);
	   	(*match)[i] =-1;
	}    
	for (i = 0; i<m; ++i) active[i]=1; //initialize  the active array 
	for (i=n-1;i>=0;--i){ //initial add to the stack of reducted edges, start in reverse   
	    if  (degs[i]==1){   //if degree is 1, then the hyperedge of this node belongs to is a possible candidate for "d-1 out of d"
	   		h = v_hids[v_starts[i]];  //find its single hyperedge 
	     	count=0;
	      	for (j = h_begs[h+1] -1;j>=h_begs[h];--j){ //for all points touched by this hyperedge, start in reverse (assume endpoints are stored in asc. order)
				x = h_points[j];  //find the j-th endpoint
				if  (degs[x] == 1){  // if its degree is also 1 
		  			if  ( x > i ) {  // if it is larger than i, it means we have already examined it before (since the loop starts in reverse order),  so we  can break the loop prematurely
		  				break;
		  			}
		 			count=count+ 1;	//otherwise we increment the count variable by one
				}		
			}
	      	if (count >= ( h_begs[h+1] - h_begs[h]  - 1)){ //if  variable count is at least  "d-1 out of d"  
				d1lst[++d1end] = h; //we add the hyper-edge ID to the stack		
			}
		}
	} 

	flag = 1;
	while (flag){      
	    while (d1end >= d1st){ //while the hyperedge array is non-empty	      
	        h = d1lst[d1st++]; //get the next hyperedge 
			if (active[h]){ //if it is still active 
				valid =1 ;	     
				count=0; 
				for (j = h_begs[h+1] -1;j>=h_begs[h];--j){   //checks to see that all node are unmatched & see if "d-1 out of d" still holds
					x = h_points[j];
					if  ((*match)[x]  != -1){
						valid=0; 
						break; //the break before the count++  ensures that if we break, count will never support the "d-1 out of d"
					}
					if (degs[x] == 1)  count++;
				} 
		   		if (valid && count >= (h_begs[h+1] -1 -h_begs[h])  ){ //if all conditions hold,  match the hyperedge
		   			matches++;
		   			(*match_set)[s_matches]=h;
		   			s_matches++;
		   			for (j = h_begs[h];j<h_begs[h+1]-1;++j){ 
		   				x1 = h_points[j];
		   				x2 = h_points[j+1];			
		   				(*match)[x1]=x2;

		   			} 
		   			(*match)[h_points[h_begs[h+1] - 1]] = h_points[h_begs[h]];
		      		active[h]=0;	 //set hyperedge non-active
		      		updateDegs(h,v_hids, v_starts,h_begs, h_points, *match, &degs,&d1lst,&d1st,&d1end,active); //update degrees for other vertices
		  		}
			}
		}    
	      //otherwise we need to select an arbitrary hypedge
		max_index = -1;
		max_coeff = -1;
		curr=randEdgeSt;
		while (curr < m){
			h=edge_list[curr];
			valid=1;
			temp_coeff=1;
			if (active[h]){
				for (j=h_begs[h+1] -1;j>=h_begs[h];--j){ //this for-loop is probably useless due to the active variable, but better safe than sorry! 
					x = h_points[j];
					temp_coeff*=degs[x];
					if  ((*match)[x]  != -1){
						valid=0; 
						break;
					}
				} 
			}
			else{ 
				valid=0;
			}
			if (valid==1){
				temp_coeff= 1.0/temp_coeff;
				if (max_coeff < temp_coeff){
					max_coeff=temp_coeff;
					max_index=curr; 
				}
				curr++;
			}
			else{
				edge_list[curr]=edge_list[m-1];
				m--;
			}
		}
		if (max_index ==-1)
			break;

		swap_temp=edge_list[max_index];
		edge_list[max_index]=edge_list[randEdgeSt];
		edge_list[randEdgeSt]=swap_temp;
		h=edge_list[randEdgeSt++];
		matches++;
		for (j = h_begs[h];j<h_begs[h+1]-1;++j){ 
			x1 = h_points[j];
			x2 = h_points[j+1];			
			(*match)[x1]=x2;
		} 
		(*match_set)[s_matches]=h;
		s_matches++;
		(*match)[h_points[h_begs[h+1] - 1]] = h_points[h_begs[h]];
		active[h]=0;	 //set hyperedge non-active
		updateDegs(h,v_hids, v_starts,h_begs, h_points, *match, &degs,&d1lst,&d1st,&d1end,active);	
	    if (randEdgeSt>=m &&  d1end < d1st) {//condition to break the loop
	      	flag = 0;	      
	    }
	}/*while flag*/
	free(active);
	free(degs);
	free(d1lst);
	if (s_matches != matches)
	   	printf("s_matches ~= matches \n");
    return matches;
}

void read_file_ks(int *n,int *m,int *k,char *filename, int **v_hids, int **v_starts, int **h_begs, int **h_points,int o,int *min_ni)
{
	  	int t_n, t_m,i,j,t_k,x,q,t_npk=0;
	  	int *degs;
	  	int kv;
	  	int *dim_starts;
	  	int mv;
	  //file is in format   n m k       n:  number of all nodes (on any size) , m the number of hyperedges, and k the number of partitions
	  //then m lines follow each in the format a_1 a_2 .... a_k  such that  h_i = (a_1 .... a_k) is the i-th hyperedge with i<m
	  //Nodes are 0-based
	  //use similar to SPARTAN 

	  	FILE *in_file=fopen(filename, "r"); 
	  	if (o==0)
	  		fscanf(in_file, "%d", &t_npk);
	  	fscanf(in_file, "%d", &t_m);
	  	fscanf(in_file, "%d", &t_k);
	  dim_starts=(int *)calloc(t_k+1,sizeof(int)); //first ID of a vertex in i-th dimension
	  t_n=0; 
	  mv=-1;	 
	  dim_starts[0]=0;
	  if (o==1){
	  	for (i=0;i<t_k;++i){
	  		fscanf(in_file, "%d", &t_npk);
	  		t_n+=t_npk;
	  		dim_starts[i+1]= dim_starts[i] + t_npk;  		
	  		if (mv==-1) mv=t_npk;
	  		if (mv > t_npk) mv=t_npk;
	  	}	
	  }else{
	  	for (i=0;i<t_k;++i)
	  		dim_starts[i]= i * t_npk ;                
	  	t_n=t_npk * t_k;
	  	mv=t_npk;
	  }    
	  *min_ni=mv;
	  *v_hids =malloc(  (t_k*t_m) * sizeof(int*)); //big list containing the hyperedge IDs of each node 
	  *v_starts=malloc( (t_n+1) * sizeof(int*));  //pointer structure that says where the IDs of the i-th node begin in the above list
	  *h_points = malloc ( t_m * t_k * sizeof(int*)); //end points for all hyperedges
	  *h_begs = malloc ( (t_m+1) * sizeof(int*));  //pointer structure that says where the endpoints of each edge begin in the above list
	  degs=(int *) calloc(t_n, sizeof(int)); //degree-array for initialization purposes
	  for (i=0;i<t_n;++i) degs[i]=0;
	  	q=0;
	  for (i =0; i<t_m;++i){ //first fill the hyperedge array information and find degree of each node
	    (*h_begs)[i]=q;   //the i-th edge begins where the (i-1) ended +1
	    for (j=0;j<t_k;++j){ //read all endpoints
	    	fscanf(in_file, "%d", &kv);
	    	degs[dim_starts[j] + kv]++; 
	      (*h_points)[q++]=dim_starts[j] + kv; //store them   
	  }    
	}
	  (*h_begs)[t_m]= q;  //last-non existing edge
	  (*v_starts)[0]=0;  //now consider the v_starts 
	  for (i=1 ; i<=t_n; ++i){ //fore ach node
	    (*v_starts)[i] = (*v_starts)[i-1] + degs[i-1]; //i-th node begins where the (i-1) node begins + the number of edges (i-1) has
	    degs[i-1]= (*v_starts)[i-1]; //set it at the beginning for the next step (just to save some space, we reuse degs array)
	} 
	  for (i=0; i < t_m ;++i){ //re-read each edge
	  	for (j=(*h_begs)[i] ; j < (*h_begs)[i+1]; ++j){
	      x = (*h_points)[j];    //store the hyperedge id to each endpoint 
	      (*v_hids)[degs[x]]=i;
	      degs[x]++; //increment degs[x] so that the next edge containing x will be saved in the correct position
	  }
	}
	*n=t_n;
	*m=t_m;
	*k=t_k;
	free(degs);

	fclose(in_file);
}
