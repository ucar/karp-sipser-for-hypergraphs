**ABOUT**

The implementation of the Karp-Sipser-mindegree algorithm with only Rule-1

**COMPILATION**

to compile: type `make` in the command line.

**USAGE**

to use: enter  `./ksmd filename o`

where

- `filename : the location of the input hypergraph`

- `o : 1 if the hypergraph has a non-equal number of vertices per dimension or 0 else`

  

  which calls a function 

```
test_ksmd(filename,o,&match,&n,&matches)
```

and with outputs

- `match[i] : -1 if the i-th vertex is unmatched, or  its pair in the next dimension`
- `n : the overall number of vertices in the hypergraph`
- `matches: the number of matching edge found by the heuristic`

Note that 

1. The vertices are ordered according to their dimension. The `id` of  `u`  from dimension `i`  in the `matched` array  is equal to `(number of vertices in dimensions 0 until (i-1))+ u`  
2.  Vertices from the last dimension  point to their pair in the first dimension.

**EXAMPLE**

- ` ./ksmd ../Data/sample_0.txt 0`
- ` ./ksmd ../Data/sample_1.txt 1`