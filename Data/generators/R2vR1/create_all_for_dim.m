function create_all_for_dim(ndims)
	mkdir(strcat('tests/',int2str(ndims)));
    fileSID=fopen(strcat('tests/',int2str(ndims),'/summary.txt'),'w');
    siz=[1000,2000,4000];
    for i=1: length(siz)
            n=siz(i);
                fname=strcat(int2str(n),'_',int2str(ndims),'.txt');
                fname2=strcat('tests/',int2str(ndims),'/',fname);
                fprintf(fileSID,'%s \n',fname);
				R2sup(fname2,n,ndims);
            
        
    end
    fclose(fileSID);
    end
