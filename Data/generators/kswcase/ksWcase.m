function A = ksWcase(n, k)
%
if ( (mod(n, 2) ~= 0) || (n < 2 * k) || k < 2),  
    error('ksWcaseUG: parameters are not ok n=%d k=%d\n', n, k);
end

A = sparse(n, n);
A(1:n/2, 1:n/2) = 1;
A(1:n/2, (n/2+1):n) = speye(n/2);
A((n/2-k+1):n/2, n/2+1:n) = 1;

A = A + A';
A = spones(A);
%A = spdiags(zeros(n,1), 0, A);
end%function
