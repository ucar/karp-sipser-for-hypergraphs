function T= createRandomHyperGraph(n , k,m)
    T=zeros(m,k);
    for i=1:m
        for j=1:k
            T(i,j)= randi([0 n-1],1,1);
        end
    end
end
    