function hedges = koutHypergraphs(n, ndims, k)
% hedges = koutHypergraphs(n, ndims, k)
% ndims dimensional, each dimension is of size n
% k hedges per vertex.
%

hedges = zeros(n*k*ndims, ndims);
selectingV = zeros(n*k, 1);
for ii = 1:n,
    selectingV((ii-1) * k+1: ii * k) = ii;
end

for d = 1:ndims,
    %mage the vertices in dimension d, kout
    hedgesD = randi([1, n], n*k, ndims);
    hedgesD(:, d) = selectingV;
    hedges((d-1) * k * n +1: d * k * n, :) = hedgesD;
end


hedges = sortrows(hedges, 1:ndims);
inoutput = false(size(hedges, 1), 1);
inoutput(1) = true;
for ii = 2:length(hedges),
    if(any(hedges(ii, :) ~= hedges(ii-1, :))),
        inoutput(ii) = true;
    end
end
hedges = hedges(inoutput, :);

end%function
    