**Effective heuristics for matchings in hypergraphs**

------

**ABOUT**

The implementation of the Karp-Sipser-scaling and Karp-Sipser-mindegree  algorithms from the paper 

F. Dufossé, K. Kaya, I. Panagiotas, and B. Uçar, “Effective Heuristics for matchings in hypergraphs”, in Proceedings of SEA<sup>2</sup> 2019, Kalamata, Greece, June 24–29, 2019, 2019, pp. 248–265

```
@inproceedings{dufosse2019effective,
  title={Effective heuristics for matchings in hypergraphs},
  author={Dufoss{\'e}, Fanny and Kaya, Kamer and Panagiotas, Ioannis and U{\c{c}}ar, Bora},
  booktitle={International Symposium on Experimental Algorithms},
  pages={248--264},
  year={2019},
  organization={Springer}
}
```

**LICENCE**

See the file LICENSE. The software is under CeCILL-B license, which is a BSD-like license.

**CONTACT**

The e-mail addresses of the authors are: [fanny.dufosse@inria.fr](mailto:fanny.dufosse@inria.fr), [kamer.kaya@sabanciuniv.edu](mailto:kamer.kaya@sabanciuniv.edu), [ioannis.panagiotas@ens-lyon.fr](mailto:ioannis.panagiotas@ens-lyon.fr), and [bora.ucar@ens-lyon.fr](mailto:bora.ucar@ens-lyon.fr).

**USAGE**

The codes work  strictly with a d-partite,  d-uniform hypergraph such that 

- The vertex set of the Hypergraph can be partitioned into d disjoint sets of vertices
- Each hyperedge contains exactly one vertex from each such set

If the given hypergraph has equal number of vertices in each partition, then the first line in the input file is as follows

`npp m k`

where

- `npp: the number of vertices per partition`
- `m: the number of hyperedges`
- `the number of partitions`

Otherwise, if the number of vertices is potentially different for each dimension, then the first line in the input file is as follows 

`m k`

where  `m,k` are defined as above.

The second line is then

`n1 n2 ... nk`

 such that `ni` is the number of vertices in the i-th partition

The next `m` lines are common for both types of input and each of them is defined as follows

`u1 u2 ... uk`   

meaning that there exists a hyperedge connecting vertex `u1`  from the first partition with `u2` from the second partition, and likewise.

Each `ui` is strictly between `0` and   `npp -1` or `ni -1`  (depending on the type of input)

Please look into the  `Data` folder for sample files. 

Detailed instructions for each algorithm maybe be found at its corresponding folder.



**FILES**

1. LICENSE: Describes the CeCILL-B license.
2. README.md: This file.
3. kss/ksscaller.c: A driver routine to call the Karp-Sipser-scaling algorithm.
4. kss/kss_utils.c: Implementation of the Karp-Sipser-scaling algorithm.
5. kss/kss_utils.h: Header definitions for the above .c file.
6. kss/Makefile: a Makefile for the Karp-Sipser-scaling algorithm.
7. kss/README.md: Readme file for  the Karp-Sipser-scaling algorithm.
8. ksmd/ksmdcaller.c: A driver routine to call the Karp-Sipser-mindegree algorithm.
9. ksmd/ksmd_utils.c: Implementation of the Karp-Sipser-mindegree algorithm.
10. ksmd/ksmd_utils.h: Header definitions for the above .c file.
11. ksmd/Makefile: a Makefile for the Karp-Sipser-mindegree algorithm.
12. ksmd/README.md: Readme file for  the Karp-Sipser-mindegree algorithm.
13. Data/sample_0.txt: A sample file with equal number of vertices per partition
14. Data/sample_1.txt: A sample file with unequal number of vertices per partition